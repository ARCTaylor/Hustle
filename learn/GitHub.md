# Resources for Learning GitHub

### Text Documentation

[GitHub Getting Started](https://docs.github.com/en/get-started)

[GitHub Education Starter Course](https://github.com/education/github-starter-course)

### Youtube Tutorials

[Programming with Mosh Git Tutorial for Beginners](https://www.youtube.com/watch?v=8JJ101D3knE)

[freeCodeCamp Git & GitHub for Beginners](https://www.youtube.com/watch?v=RGOj5yH7evk)
