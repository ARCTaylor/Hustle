# Resources for Learning CSS

### Text Documentation

[Mozilla Developer CSS First Steps](https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps)

[devdocs.io CSS Reference](https://devdocs.io/css/)

[Adam Marsden CSS Cheat Sheet](https://adam-marsden.co.uk/css-cheat-sheet)

### Youtube Tutorials

[freeCodeCamp CSS Zero to Hero Tutorial](https://www.youtube.com/watch?v=1Rs2ND1ryYc)

[Web Dev Simplified Introduction to CSS Playlist](https://www.youtube.com/watch?v=1PnVor36_40&list=PLZlA0Gpn_vH9D0J0Mtp6lIiD_8046k3si)
