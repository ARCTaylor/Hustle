# Resources for Learning JavaScript

###

[Mozilla Developer JavaScript First Steps](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps)

[devdocs.io JavaScript Reference](https://devdocs.io/javascript/)

[The Modern JavaScript Tutorial](https://javascript.info/)

### Youtube Tutorials

[Coding Train Beginner Programming Tutorial Playlist](https://www.youtube.com/watch?v=HerCR8bw_GE&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA)

[Coding Train Data & API Playlist](https://www.youtube.com/watch?v=DbcLg8nRWEg&list=PLRqwX-V7Uu6YxDKpFzf_2D84p0cyk4T7X)
