# Resources for Learning HTML

### Text Documentation

[Mozilla Developer Intro to HTML](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML)

[devdocs.io HTML Reference](https://devdocs.io/html/)

### Youtube Tutorials

[Programming with Mosh HTML Tutorial for Beginners](https://www.youtube.com/watch?v=qz0aGYrrlhU)

[freeCodeCamp HTML Tutorial](https://www.youtube.com/watch?v=kUMe1FH4CHE)
