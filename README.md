# Hustle
Social Media Planner App


## Conventions for Using this Repo
Do NOT push code directly to the main branch. Please create a pull request.  
Do NOT preemptively dismiss issues. These are an integral part of our project management.

#### Our Git Development Flow
1) Pull from remote repository if necessary to get latest version of code
2) Create new feature branch if necessary. Otherwise checkout relevant branch.
3) Make changes/develop feature
4) Stage, commit changes to feature branch
5) Create pull request to integrate feature into main branch

## What Tools do I need for Development?

#### The Tools We Use
IDE: [VSCode](https://code.visualstudio.com/) (download this)  
Framework: [Quasar](https://quasar.dev/)  
Hosting: [AWS Amplify](https://aws.amazon.com/amplify/)  
Database: TBD (most likely AWS RDS or MongoDB)  
Authentication: [Userfront](https://userfront.com/)  
Tooling: yarn, prettier  
other: [Node JS](https://nodejs.org/en/) (download this)  

#### Setting Things Up 
1) Make sure you have Node.js and VSC installed as detailed above
2) In VSC, navigate to the source control panel on the left bar. Click the `...` and click `Clone`
3) Either paste in the repo HTTPS (`https://github.com/ARCTaylor-Development/Hustle.git`) or follow the prompts to sign in to GitHub and clone the repo
4) Open a VSC terminal through the top options bar, change directory with `cd code/quasar` and run `npm install` to download the dependancy packages
5) Run `npm install --global yarn` to install yarn
6) Run `yarn global add @quasar/cli` to install the Quasar CLI to your project
7) You can serve the site locally by running `yarn quasar dev`, which should host the project locally at `http://localhost:9000`

## What Skills do I need for Development?
Please refer to the `learn` folder for resources for learning HTML, CSS, JS, and more.  
Communication, teamwork, perseverance, and patience

